import React from 'react'
import './controlPanel.less'
class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: []
    }
  }

  handleClick(even) {
    const str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const num = Math.floor(Math.random() * 26);
    let array = new Array();
    for (let i = 0; i < 4; i++) {
      array.push(str.charAt(Math.floor(Math.random() * 26)))
    }
    this.setState({
      answer: array
    }, () => {
      console.log('随机数:'+this.state.answer)
    })
    this.props.handleChange(array);
  }


  render() {
    return (
      <div className="control">
        <button className="newCart" onClick={this.handleClick.bind(this)}>New Card</button><br/>
        <input type="text" value={this.state.answer[0]}/>
        <input type="text" value={this.state.answer[1]}/>
        <input type="text" value={this.state.answer[2]}/>
        <input type="text" value={this.state.answer[3]}/>
      </div>
    )
  }
}

export default ControlPanel