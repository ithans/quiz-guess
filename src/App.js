import React from 'react'
import ControlPanel from "./ControlPanel";
import PlayingPanel from "./PlayingPanel";
import './App.less'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answers: []
    }
    this.handleChange=this.handleChange.bind(this);
  }

  handleChange(array) {
    this.setState({
      answers: array[0]+array[1]+array[2]+array[3]
    }, () => {
      console.log('app' + this.state.answers);
    })

  }

  render() {
    return (
      <div>
        <ControlPanel handleChange={this.handleChange}></ControlPanel>
        <PlayingPanel answer={this.state.answers}></PlayingPanel>
      </div>
    )
  }
}

export default App