import React from 'react'
import './playingPanel.less'
class PlayingPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      guessNum: "",
      status:""
    }
  }
  handleChange(even){
    this.setState({
      guessNum:even.target.value
    })
  }
  handleClick(even){
    if(this.props.answer === this.state.guessNum){
      this.setState({
        status:'SUCCESS'
      },()=>{
        console.log(this.state.status)
      })
    }else{
      this.setState({
        status:'FAILED'
      },()=>{
        console.log(this.state.status)
      })
    }
  }

  render() {
    return (
      <div>
          <div className='guess'>
            <input className="inputGuess" type="text" maxLength="4" min="4" onChange={this.handleChange.bind(this)}/>
            <button className='guessButton' onClick={this.handleClick.bind(this)}>Guess</button>
          </div>
          <div className='check'>
            <input type="text" maxLength="0" value={this.state.guessNum.charAt(0)} />
            <input type="text" maxLength="1" value={this.state.guessNum.charAt(1)}/>
            <input type="text" maxLength="2" value={this.state.guessNum.charAt(2)}/>
            <input type="text" maxLength="3" value={this.state.guessNum.charAt(3)}/>
            <label>{this.state.status}</label>
          </div>
      </div>
    )
  }
}

export default PlayingPanel